﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.AccessControl;
using System.Text;
using System.Windows.Forms;

namespace EMail_Sender
{
    public partial class FormSetting : Form
    {
        private SenderSetting ss;
        private string pathFileSetting;
        public FormSetting(SenderSetting ss, string pathFileSetting)
        {
            this.ss = ss;
            this.pathFileSetting = pathFileSetting;
            InitializeComponent();
            this.textBoxPassword.UseSystemPasswordChar = true;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!File.Exists(this.pathFileSetting))
            {
                using (FileStream fstream = new FileStream(this.pathFileSetting, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    // Создание файла
                }
            }
            try
            {

                var polybius = new CryptoStr();

                FileINI.Write(this.pathFileSetting, "Global", "from", polybius.PolibiusEncrypt(textBoxFrom.Text));
                FileINI.Write(this.pathFileSetting, "Global", "to", polybius.PolibiusEncrypt(textBoxTo.Text));
                FileINI.Write(this.pathFileSetting, "Global", "smtp", textBoxSmtp.Text);
                FileINI.Write(this.pathFileSetting, "Global", "port", textBoxPort.Text);
                FileINI.Write(this.pathFileSetting, "Global", "username", polybius.PolibiusEncrypt(textBoxUsername.Text));
                FileINI.Write(this.pathFileSetting, "Global", "password", polybius.PolibiusEncrypt(textBoxPassword.Text));
                FileINI.Write(this.pathFileSetting, "Global", "addPhone", polybius.PolibiusEncrypt(textBoxAddPhone.Text));
                FileINI.Write(this.pathFileSetting, "Global", "addPC", polybius.PolibiusEncrypt(textBoxNameAddPC.Text));
                FileINI.Write(this.pathFileSetting, "Global", "addText", polybius.PolibiusEncrypt(richTextBoxAddText.Text));
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ошибка!\n" + ex.Message, "Результат", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }

        public SenderSetting getNewSenderSettin()
        {
            return this.ss;
        }
    }
}
