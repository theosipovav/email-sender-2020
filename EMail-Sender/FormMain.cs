﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

/*
 * Должна работать на системе windows XP, 7, 8, 10 всех модификаций.
 * Программа должна уметь так же работать с шифрованием данных с помощью протоколов SSL и TLS (порты 25 и 465).
 * Уметь отправлять вложения и поддерживать вставку картинок (скриншотов, CTRL+V).
 * В настройках программы необходимо предусмотреть следущие поля 
 * настройки smtp, 
 * Получатель (e-mail), 
 * Пользователь(текст), Телефон (текст), Имя компьютера (текст), Дополнительно (текст). Поля Пользователь, Телефон, Имя компьютера, Дополнительно должны будут отправляться в начале каждого письма. На основной форме приложения должно быть текстовое поле и кнопка отправить.
 */


namespace EMail_Sender
{
    public partial class FormMain : Form
    {
        SenderSetting ss = new SenderSetting();
        string pathFileSetting = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "\\EMail-Sender.ini";
        List<string> listAttachment = new List<string>();
        public FormMain()
        {
            InitializeComponent();
            flowLayoutPanel1.FlowDirection = FlowDirection.LeftToRight;
            this.AllowDrop = true;
            flowLayoutPanel1.AllowDrop = true;
            loadSetting();

        }

        private bool loadSetting()
        {
            if (!File.Exists(this.pathFileSetting))
            {
                // Настройки по умолчанию
                if (MessageBox.Show("Файл настроек не обнаружен!\nИспользовать настройки по умолчанию?)", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    this.ss.settingDefault();
                }
                else
                {
                    FormSetting formSetting = new FormSetting(this.ss, this.pathFileSetting);
                    if (formSetting.ShowDialog() == DialogResult.OK)
                    {
                        loadSetting();
                    }
                }
            }
            else
            {
                // Настройки берутся из файла настройки
                if (FileINI.KeyExists(this.pathFileSetting, "from") &&
                    FileINI.KeyExists(this.pathFileSetting, "to") &&
                    FileINI.KeyExists(this.pathFileSetting, "smtp") &&
                    FileINI.KeyExists(this.pathFileSetting, "port") &&
                    FileINI.KeyExists(this.pathFileSetting, "username") &&
                    FileINI.KeyExists(this.pathFileSetting, "password") &&
                    FileINI.KeyExists(this.pathFileSetting, "addPhone") &&
                    FileINI.KeyExists(this.pathFileSetting, "addPC") &&
                    FileINI.KeyExists(this.pathFileSetting, "addText"))
                {
                    // В файле настройки присутствуют все необходимые параметры
                    var polybius = new CryptoStr();
                    this.ss.from = polybius.PolybiusDecrypt(FileINI.Read(this.pathFileSetting, "Global", "from"));
                    this.ss.to = polybius.PolybiusDecrypt(FileINI.Read(this.pathFileSetting, "Global", "to"));
                    this.ss.smtp = FileINI.Read(this.pathFileSetting, "Global", "smtp");
                    this.ss.port = FileINI.ReadToInt32(this.pathFileSetting, "Global", "port");
                    if (this.ss.port == null || this.ss.port == 0) this.ss.port = 587;
                    this.ss.username = polybius.PolybiusDecrypt(FileINI.Read(this.pathFileSetting, "Global", "username"));
                    this.ss.password = polybius.PolybiusDecrypt(FileINI.Read(this.pathFileSetting, "Global", "password"));
                    this.ss.addPhone = polybius.PolybiusDecrypt(FileINI.Read(this.pathFileSetting, "Global", "addPhone"));
                    this.ss.addPC = polybius.PolybiusDecrypt(FileINI.Read(this.pathFileSetting, "Global", "addPC"));
                    this.ss.addText = polybius.PolybiusDecrypt(FileINI.Read(this.pathFileSetting, "Global", "addText"));
                }
                else
                {
                    // В файле настройки отсутствуют необходимые параметры
                    MessageBox.Show("В файле настройки отсутствуют необходимые параметры.\nФайл будет удален.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    File.Delete(this.pathFileSetting);
                    Environment.Exit(0);

                }

            }
            return true;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            
            try
            {
                //
                string msg = richTextBox1.Text;
                msg += "\n";
                msg += "\nТелефон: " + ss.addPhone;
                msg += "\nКомпьютер: " + ss.addPC;
                msg += "\n" + ss.addText;
                //
                MailMessage mail = new MailMessage(ss.from, ss.to, textBoxSubject.Text, msg);
                SmtpClient client = new SmtpClient(ss.smtp, 25);
                client.Credentials = new System.Net.NetworkCredential(ss.username, ss.password);      
                client.EnableSsl = true;
                // Вложения
                foreach (string path in listAttachment)
                {
                    mail.Attachments.Add(new Attachment(path.ToString()));
                }

                client.Send(mail);
                MessageBox.Show("Сообщение отправлено!", "Результат", MessageBoxButtons.OK, MessageBoxIcon.Question);
                this.textBoxSubject.Text = "";
                this.richTextBox1.Text = "";
                listAttachment.Clear();
                flowLayoutPanel1.Controls.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка!\n"+ ex.Message + "\n" + ex.Source + "\n" + ex.HelpLink, "Результат", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void toolStripButtonSetting_Click(object sender, EventArgs e)
        {
            FormSetting formSetting = new FormSetting(this.ss, this.pathFileSetting);
            if (formSetting.ShowDialog() == DialogResult.OK)
            {
                loadSetting();
            }
        }

        private void toolStripButtonExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);

        }

        private void buttonAttached_Click(object sender, EventArgs e)
        {

            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Multiselect = true;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    foreach (string fileNameBefore in dialog.FileNames)
                    {
                        string fileName = fileNameBefore.ToUpper();

                        bool isBeforeAttached = false;
                        foreach (string path in listAttachment)
                        {
                            if (fileName == path)
                            {
                                isBeforeAttached = true;
                                break;
                            }
                        }
                        if (isBeforeAttached)
                        {
                            continue;
                        }

                        listAttachment.Add(fileName);
                        Button button = new Button();
                        button.Click += new System.EventHandler(this.buttonAttachmentFile_Click);
                        button.Text = Path.GetFileName(fileName);
                        button.AutoSize = true;
                        flowLayoutPanel1.Controls.Add(button);
                        flowLayoutPanel1.BackgroundImage = null;

                    }


                }
            }
        }


        private void buttonAttachmentFile_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            foreach (string path in listAttachment)
            {
                if (Path.GetFileName(path) == button.Text)
                {
                    listAttachment.RemoveAt(listAttachment.IndexOf(path));
                    break;
                }
            }

            button.Dispose();
        }

        private void buttonDeleteAllAttachmentFiles_Click(object sender, EventArgs e)
        {
            listAttachment.Clear();
            flowLayoutPanel1.Controls.Clear();

        }

        private void flowLayoutPanel1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string fileBefore in files)
            {
                string file = fileBefore.ToUpper();
                bool isBeforeAttached = false;
                foreach (string path in listAttachment)
                {
                    if (file == path)
                    {
                        isBeforeAttached = true;
                        break;
                    }
                }
                if (isBeforeAttached)
                {
                    continue;
                }

                listAttachment.Add(file);
                Button button = new Button();
                button.Click += new System.EventHandler(this.buttonAttachmentFile_Click);
                button.Text = Path.GetFileName(file);
                flowLayoutPanel1.Controls.Add(button);
                flowLayoutPanel1.BackgroundImage = null;
            }
        }

        private void flowLayoutPanel1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;

        }

        private void richTextBox1_KeyPress(object sender, KeyEventArgs e)
        {
            if (e.Control == true && e.KeyCode == Keys.V)
            {
                if (Clipboard.GetImage() != null)
                {
                    Image image = null;
                    image = Clipboard.GetImage();
                    string file = Path.GetTempPath() + Guid.NewGuid().ToString() + ".png";
                    image.Save(file.ToUpper());
                    listAttachment.Add(file);
                    Button button = new Button();
                    button.Click += new System.EventHandler(this.buttonAttachmentFile_Click);
                    button.Text = Path.GetFileName(file);
                    flowLayoutPanel1.Controls.Add(button);
                    flowLayoutPanel1.BackgroundImage = null;
                }
            }

        }

        private void toolStripButtonSettingDefault_Click(object sender, EventArgs e)
        {
            File.Delete(this.pathFileSetting);
            this.ss.settingDefault();
        }
    }
}
