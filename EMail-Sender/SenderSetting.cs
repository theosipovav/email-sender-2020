﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMail_Sender
{
    public class SenderSetting
    {
        /// <summary>
        /// Отправитель
        /// </summary>
        public string from { get; set; }
        /// <summary>
        /// Получатель
        /// </summary>
        public string to { get; set; }
        /// <summary>
        /// Адрес почтового сервера SMTP
        /// </summary>
        public string smtp { get; set; }
        /// <summary>
        /// Порт
        /// </summary>
        public int? port { get; set; }
        /// <summary>
        /// Логин пользователя исходящей почты
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// Пароль пользователя исходящей почты
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// Протокол SSL
        /// </summary>
        public bool ssl { get; set; }
        /// <summary>
        /// Дополнительный текст: телефон
        /// </summary>
        public string addPhone { get; set; }
        /// <summary>
        /// Дополнительный текст: имя ПК
        /// </summary>
        public string addPC { get; set; }
        /// <summary>
        /// Дополнительный текст: текст
        /// </summary>
        public string addText { get; set; }


        public void settingDefault()
        {
            this.from = "test@test.ru";     // Отправитель
            this.to = "test@test.ru";       // Получатель
            this.smtp = "smtp.test.ru";     // Адрес почтового сервера SMTP
            this.username = "test@test.ru"; // Логин пользователя исходящей почты
            this.password = "test"; // Пароль пользователя исходящей почты
            this.addPhone = "test"; // Дополнительный текст: телефон
            this.addPC = "test";    // Дополнительный текст: имя ПК
            this.addText = "test";  // Дополнительный текст: текст
            this.port = null;   // порт (если 465, то оставить NULL)
        }
    }
}
